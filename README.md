---
title: Profiling the Standard Library
subtitle: Using Google Benchmark
author: Dean Turpin
---

See [the repo](https://gitlab.com/germs-dev/benchmark) for this project and check out the [the Google Benchmark docs](https://github.com/google/benchmark/blob/main/docs/user_guide.md).

