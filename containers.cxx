#include <benchmark/benchmark.h>

#include <deque>         // vector of fixed-sized vectors
#include <forward_list>  // Singly linked list
#include <list>          // Doubly linked list
#include <map>           // RB tree
#include <set>           // RB tree
#include <unordered_map> // Hashmap
#include <unordered_set> // Hashmap
#include <vector>        // Contiguous storage
// #include <flat_map>    // not implemented yet

constexpr auto num_elements{1uz};

static void vector_insert_at_front(benchmark::State &state) {
  for (auto _ : state) {
    std::vector<int> x;
    int v = 0;
    for (auto i = 0u; i < num_elements; ++i)
      x.insert(x.begin(), v++);
  }
}

static void vector_insert_at_back(benchmark::State &state) {
  for (auto _ : state) {
    std::vector<int> x;
    int v = 0;
    for (auto i = 0u; i < num_elements; ++i)
      x.push_back(v++);
  }
}

static void vector_insert_at_front_with_reserve(benchmark::State &state) {
  for (auto _ : state) {
    std::vector<int> x;
    x.reserve(num_elements);
    int v = 0;
    for (auto i = 0u; i < num_elements; ++i)
      x.insert(x.begin(), v++);
  }
}

static void vector_insert_at_back_with_reserve(benchmark::State &state) {
  for (auto _ : state) {
    std::vector<int> x;
    x.reserve(num_elements);
    int v = 0;
    for (auto i = 0u; i < num_elements; ++i)
      x.push_back(v++);
  }
}

static void deque_insert_at_front(benchmark::State &state) {
  for (auto _ : state) {
    std::deque<int> x;
    int v = 0;
    for (auto i = 0u; i < num_elements; ++i)
      x.push_front(v++);
  }
}

static void deque_insert_at_back(benchmark::State &state) {
  for (auto _ : state) {
    std::deque<int> x;
    int v = 0;
    for (auto i = 0u; i < num_elements; ++i)
      x.push_back(v++);
  }
}

static void list_insert_at_front(benchmark::State &state) {
  for (auto _ : state) {
    std::list<int> x;
    int v = 0;
    for (auto i = 0u; i < num_elements; ++i)
      x.push_front(v++);
  }
}

static void list_insert_at_back(benchmark::State &state) {
  for (auto _ : state) {
    std::list<int> x;
    int v = 0;
    for (auto i = 0u; i < num_elements; ++i)
      x.push_back(v++);
  }
}

static void forward_list_insert_at_front(benchmark::State &state) {
  for (auto _ : state) {
    std::forward_list<int> x;
    int v = 0;
    for (auto i = 0u; i < num_elements; ++i)
      x.push_front(v++);
  }
}

static void set_insert(benchmark::State &state) {
  for (auto _ : state) {
    std::set<int> x;
    int v = 0;
    for (auto i = 0u; i < num_elements; ++i) {
      x.insert(v);
      ++v;
    }
  }
}

static void unordered_set_insert(benchmark::State &state) {
  for (auto _ : state) {
    std::unordered_set<int> x;
    int v = 0;
    for (auto i = 0u; i < num_elements; ++i) {
      x.insert(v);
      ++v;
    }
  }
}

static void unordered_map_insert(benchmark::State &state) {
  for (auto _ : state) {
    std::unordered_map<int, int> x;
    int v = 0;
    for (auto i = 0u; i < num_elements; ++i) {
      x.insert({v, v});
      ++v;
    }
  }
}

static void map_insert(benchmark::State &state) {
  for (auto _ : state) {
    std::map<int, int> x;
    int v = 0;
    for (auto i = 0u; i < num_elements; ++i) {
      x[v] = v;
      ++v;
    }
  }
}

BENCHMARK(vector_insert_at_front);
BENCHMARK(vector_insert_at_back);
BENCHMARK(vector_insert_at_front_with_reserve);
BENCHMARK(vector_insert_at_back_with_reserve);
BENCHMARK(deque_insert_at_front);
BENCHMARK(deque_insert_at_back);
BENCHMARK(list_insert_at_front);
BENCHMARK(list_insert_at_back);
BENCHMARK(forward_list_insert_at_front);
BENCHMARK(set_insert);
BENCHMARK(map_insert);
BENCHMARK(unordered_set_insert);
BENCHMARK(unordered_map_insert);

