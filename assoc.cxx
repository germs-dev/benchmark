#include <benchmark/benchmark.h>

#include <set>
#include <string>
#include <unordered_set>

const std::string element{"hello"};

static void set_with_new_element(benchmark::State &state) {
  std::set<std::string> b;
  for (auto _ : state)
    b.insert(element);
}

static void set_with_existing_element(benchmark::State &state) {
  std::set<std::string> b{element};
  for (auto _ : state)
    b.insert(element);
}

static void unordered_set_with_new_element(benchmark::State &state) {
  std::unordered_set<std::string> b;
  for (auto _ : state)
    b.insert(element);
}

static void unordered_set_with_existing_element(benchmark::State &state) {
  std::unordered_set<std::string> b{element};
  for (auto _ : state)
    b.insert(element);
}

BENCHMARK(set_with_new_element);
BENCHMARK(set_with_existing_element);
BENCHMARK(unordered_set_with_new_element);
BENCHMARK(unordered_set_with_existing_element);
