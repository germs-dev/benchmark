#include <benchmark/benchmark.h>
#include <chrono>
#include <future>
#include <thread>

// Test routines: one that doesn't do much, another that sleeps for a fixed
// duration
const auto noop_func = [] { return true; };

// A "do nothing" benchmark
static void noop_baseline(benchmark::State &state) {
  for (auto _ : state)
    ;
}

// Create threads with noop
static void noop_async(benchmark::State &state) {
  for (auto _ : state)
    std::async(std::launch::async, noop_func).get();
}

static void noop_thread(benchmark::State &state) {
  for (auto _ : state) {
    std::thread t(noop_func);
    t.join();
  }
}

static void noop_jthread(benchmark::State &state) {
  for (auto _ : state) {
    std::jthread t(noop_func);
    t.join();
  }
}

// Register our routines with Google Benchmark
BENCHMARK(noop_baseline);
BENCHMARK(noop_async);
BENCHMARK(noop_thread);
BENCHMARK(noop_jthread);
