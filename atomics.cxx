#include <atomic>
#include <benchmark/benchmark.h>
static void variable_increment_with_atomic(benchmark::State &state) {
  std::atomic<size_t> i = 0uz;
  for (auto _ : state)
    ++i;
}

static void variable_increment(benchmark::State &state) {
  size_t i = 0uz;
  for (auto _ : state)
    ++i;
}

#include <mutex>
static void variable_increment_with_mutex(benchmark::State &state) {
  for (auto _ : state) {
    size_t i = 0uz;
    std::mutex m;

    // std::scoped_lock lock(m);
    m.lock();
    ++i;
    m.unlock();
  }
}

static void variable_increment_with_scoped_lock(benchmark::State &state) {
  for (auto _ : state) {
    size_t i = 0uz;
    std::mutex m;

    std::scoped_lock lock(m);
    ++i;
  }
}

BENCHMARK(variable_increment);
BENCHMARK(variable_increment_with_atomic);
BENCHMARK(variable_increment_with_mutex);
BENCHMARK(variable_increment_with_scoped_lock);
